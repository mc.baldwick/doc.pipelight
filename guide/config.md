# Configuration in depth

## Typings

Supports Typescript.
Import type definition from npm package.

```ts
//pipelight.config.ts
import { Config } from "npm:pipelight";
const config: Config = {
  pipelines: [
    {
      name: "test",
      steps: [
        {
          name: "build",
          commands: ["yarn install", "yarn build"]
        }
      ],
      trigger: {
        branches: ["master", "dev"],
        actions: ["pre-push", "pre-commit"]
      }
    }
  ]
};
export default config;
```

## Typescript types definition

Here is the complete type definition

```ts
//mod.ts from the npm package
type Config = {
  pipelines?: Pipeline[];
};
type Pipeline = {
  name: string;
  steps: StepOrParallel[];
  triggers?: Trigger[];
  on_failure?: StepOrParallel[];
  on_success?: StepOrParallel[];
  on_abortion?: StepOrParallel[];
};

type StepOrParallel = Step | Parallel;
type Parallel = {
  parallel: Step[];
  on_failure?: StepOrParallel[];
  on_success?: StepOrParallel[];
  on_abortion?: StepOrParallel[];
};
type Step = {
  non_blocking?: boolean;
  name: string;
  commands: string[];
  on_failure?: StepOrParallel[];
  on_success?: StepOrParallel[];
  on_abortion?: StepOrParallel[];
};
type Trigger = {
  branches?: string[];
  actions?: Action[];
};
type Action =
  | "applypatch-msg"
  | "pre-applypatch"
  | "post-apply-patch"
  | "pre-commit"
  | "prepare-commit-msg"
  | "commit-msg"
  | "post-commit"
  | "pre-rebase"
  | "post-checkout"
  | "post-merge"
  | "pre-receive"
  | "update"
  | "post-receive"
  | "post-update"
  | "pre-auto-gc"
  | "post-rewrite"
  | "pre-push"
  | "manual";

export type {
  Config,
  Pipeline,
  StepOrParallel,
  Step,
  Parallel,
  Action,
  Trigger
};
```

## Parallel steps execution

Trigger parallel execution by declaring a Parallel object that contains multiple Steps.

```ts
//pipelight.config.ts
pipelines: [
  {
    name: "my_test",
    steps: [
      {
        parallel: [
          {
            name: "first",
            commands: [...my_commands]
          },
          {
            name: "second",
            commands: [...my_commands]
          }
        ]
      }
    ]
  }
];
```

## Non-Blocking Step execution

A non_blocking step will not stop pipeline execution on failure.

Here **second** step will always be executed even if **first** step fails.

```ts
//pipelight.config.ts
pipelines: [
  {
    name: "my_test",
    steps: [
      {
        name: "first",
        non_blocking: true,
        commands: [...my_commands]
      },
      {
        name: "second",
        commands: [...my_commands]
      }
    ]
  }
];
```

## Pipeline Fallbacks

Pipelines and steps have special fallbacks:

- on_failure
- on_success
- on_abortion

```ts
//pipelight.config.ts
const steps: Step[] = [
  {
    name: "send me an email",
    commands: ""
  }
];
const config: Config = {
  pipelines: [
    {
      name: "my_test",
      on_failure: steps,
      on_success: steps,
      on_abortion: steps,
      steps: [
        {
          name: "first",
          commands: [...my_commands]
        },
        {
          name: "second",
          commands: [...my_commands]
        }
      ]
    }
  ]
};
```
