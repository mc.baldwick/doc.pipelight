import type { DefaultTheme } from "@/config";

export const nav: DefaultTheme.Config["nav"] = [
  {
    text: "Guide",
    link: "/guide/"
  },
  {
    text: "Cookbook",
    link: "/cookbook/tips"
  },
  {
    text: "Contacts",
    link: "/others/contacts"
  },
  {
    text: "Gitea",
    link: "https://gitea.com/pipelight/pipelight"
  }
];
