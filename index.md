---

home: true
heroImage: /images/pipelight.png
actionText: Get Started
actionLink: /guide/

altActionText: Why pipelight?
altActionLink: /guide/why

features:

- title: 🤌 Typescript { Bash }
  details: Enjoy wrapping your Bash with Typescript beautiful synthax.
- title: 🚦 Automatic Trigger
  details: Trigger your scripts in background on certain events without living your editor or terminal.
- title: 🫦 Pretty Logs
  details: Easily troubleshoot your scripts with pretty and verbose logs right in your terminal.
- title: 💡 Edit pipelines quickly!
  details: As long as you know Typescript synthax and a bit of Bash,.. you are good to go.
- title: 🏡 Keep it Local
  details: Big tech out of my code please! We don't need neither heavy sofware nor online account to do CICD.
- title: 🛠️ Easy install/Uninstall
  details: From source or from your distro repos.

## footer: MIT Licensed | Copyright © 2019-present Areskul
